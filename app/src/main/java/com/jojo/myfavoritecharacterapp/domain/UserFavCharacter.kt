package com.jojo.myfavoritecharacterapp.domain


data class UserFavCharacter(
    val id: Int = 0,
    val char_id: Int,
    val user_id: Int,
    val characterName: String
)