package com.jojo.myfavoritecharacterapp.domain



data class FavoriteCharacter(
    val id: Int,
    val name: String,
    val img: String
)