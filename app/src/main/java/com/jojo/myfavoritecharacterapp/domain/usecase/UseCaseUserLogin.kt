package com.jojo.myfavoritecharacterapp.domain.usecase

import com.jojo.myfavoritecharacterapp.data.Repository
import com.jojo.myfavoritecharacterapp.data.toUser

class UseCaseUserLogin(private val itemDao: Repository,
                       private val userName: String,
                       private val userPassword: String): UseCaseCommand {



    override suspend fun <T> execute(): T {
        return itemDao.getUserLogin(userName,userPassword)?.toUser() as T
    }
}