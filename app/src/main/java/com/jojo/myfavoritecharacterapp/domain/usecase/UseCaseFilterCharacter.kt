package com.jojo.myfavoritecharacterapp.domain.usecase

import com.jojo.myfavoritecharacterapp.data.toFavoriteCharacter
import com.jojo.myfavoritecharacterapp.framework.CharacterApi


class UseCaseFilterCharacter(private val name: String): UseCaseCommand {

    override suspend fun <T> execute(): T {
        return CharacterApi.ApiService.filterCharacter(name).results.map { it.toFavoriteCharacter() } as T
    }


}