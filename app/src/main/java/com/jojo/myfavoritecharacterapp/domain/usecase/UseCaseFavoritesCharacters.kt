package com.jojo.myfavoritecharacterapp.domain.usecase

import com.jojo.myfavoritecharacterapp.data.Repository
import com.jojo.myfavoritecharacterapp.data.toUserFavCharacter

class UseCaseFavoritesCharacters(
    private val itemDao: Repository,
    private val id: Int): UseCaseCommand {

    override suspend fun <T> execute(): T {
        return itemDao.getFavoritesCharacters(id).map { it.toUserFavCharacter() } as T
    }
}