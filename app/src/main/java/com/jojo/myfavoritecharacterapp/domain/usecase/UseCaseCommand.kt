package com.jojo.myfavoritecharacterapp.domain.usecase

import java.util.*

interface UseCaseCommand {

    suspend fun <T> execute(): T

}