package com.jojo.myfavoritecharacterapp.domain.usecase

import com.jojo.myfavoritecharacterapp.data.toFavoriteCharacter
import com.jojo.myfavoritecharacterapp.framework.CharacterApi

class UseCaseDetailCharacter(private val character_id: Int): UseCaseCommand {
    override suspend fun <T> execute(): T {
        return CharacterApi.ApiService.getCharacter(character_id).toFavoriteCharacter() as T
    }
}