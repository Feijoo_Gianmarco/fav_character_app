package com.jojo.myfavoritecharacterapp.domain

data class User(

    val id: Int = 0,

    val name: String,

    val userName: String,

    val userPassword: String
)