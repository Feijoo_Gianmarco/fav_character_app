package com.jojo.myfavoritecharacterapp.framework

import com.jojo.myfavoritecharacterapp.data.FavoriteCharacterDto
import com.jojo.myfavoritecharacterapp.data.FilterResults
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val BASE_URL = "https://rickandmortyapi.com/api/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface CharacterApiService{

    @GET("character/")
    suspend fun filterCharacter(@Query("name") name: String): FilterResults

    @GET("character/{id}")
    suspend fun getCharacter(@Path("id") id: Int): FavoriteCharacterDto
}

object CharacterApi{
    val ApiService: CharacterApiService by lazy {
        retrofit.create(CharacterApiService::class.java)
    }
}