package com.jojo.myfavoritecharacterapp.framework

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jojo.myfavoritecharacterapp.data.Repository
import com.jojo.myfavoritecharacterapp.data.UserDto
import com.jojo.myfavoritecharacterapp.data.UserFavCharacterDto
import com.jojo.myfavoritecharacterapp.domain.User
import com.jojo.myfavoritecharacterapp.domain.UserFavCharacter

@Database(entities = [UserDto::class,UserFavCharacterDto::class], version = 3, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract fun repository(): Repository

    companion object{

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase{
            return INSTANCE ?: synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "character_database"
                ).fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance

                return instance
            }

        }

    }

}