package com.jojo.myfavoritecharacterapp.framework

import android.app.Application

class FavCharacterApplication: Application() {

    val database: AppDatabase by lazy { AppDatabase.getDatabase(this) }

}