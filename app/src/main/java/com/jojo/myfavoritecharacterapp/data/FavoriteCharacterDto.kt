package com.jojo.myfavoritecharacterapp.data

import com.jojo.myfavoritecharacterapp.domain.FavoriteCharacter
import com.squareup.moshi.Json

data class FilterResults(
    val results: List<FavoriteCharacterDto>
)




data class FavoriteCharacterDto(
    val id: Int,
    val name: String,
    @Json(name = "image") val img: String
)


fun FavoriteCharacterDto.toFavoriteCharacter():FavoriteCharacter{
    return FavoriteCharacter(
        id = id,
        name = name,
        img = img
    )
}