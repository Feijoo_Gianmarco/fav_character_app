package com.jojo.myfavoritecharacterapp.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jojo.myfavoritecharacterapp.domain.UserFavCharacter

@Entity
data class UserFavCharacterDto (
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name = "character_id")
    val char_id: Int,
    @ColumnInfo(name = "userId")
    val user_id: Int,
    @ColumnInfo(name = "name")
    val characterName: String
)

fun UserFavCharacterDto.toUserFavCharacter(): UserFavCharacter{
    return UserFavCharacter(
        id = id,
        char_id = char_id,
        user_id = user_id,
        characterName = characterName
    )
}