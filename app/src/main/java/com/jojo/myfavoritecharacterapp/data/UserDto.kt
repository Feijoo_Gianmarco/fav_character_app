package com.jojo.myfavoritecharacterapp.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jojo.myfavoritecharacterapp.domain.User

@Entity
data class UserDto(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "username")
    val userName: String,
    @ColumnInfo(name = "userpassword")
    val userPassword: String
)

fun UserDto.toUser(): User{
    return User(
        id = id,
        name = name,
        userName = userName,
        userPassword = userPassword
    )
}