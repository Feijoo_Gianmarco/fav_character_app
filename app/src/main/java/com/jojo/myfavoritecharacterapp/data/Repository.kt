package com.jojo.myfavoritecharacterapp.data

import androidx.room.*
import com.jojo.myfavoritecharacterapp.domain.User
import com.jojo.myfavoritecharacterapp.domain.UserFavCharacter
import kotlinx.coroutines.flow.Flow

@Dao
interface Repository {

    @Query("SELECT * FROM UserDto WHERE username = :userName AND userpassword = :password")
    suspend fun getUserLogin(userName: String, password: String): UserDto?

    @Query("SELECT * FROM userfavcharacterdto WHERE userId = :userid")
    fun getFavoritesCharacters(userid: Int): List<UserFavCharacterDto>

    @Insert
    suspend fun insertUser(user: UserDto)

    @Insert
    suspend fun addNewCharacter(character: UserFavCharacterDto)

    @Delete
    suspend fun deleteCharacter(character: UserFavCharacterDto)

}