package com.jojo.myfavoritecharacterapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jojo.myfavoritecharacterapp.databinding.ItemFilterCharacterBinding
import com.jojo.myfavoritecharacterapp.domain.FavoriteCharacter

class CharactersListAdapter(private val onItemClicked: (FavoriteCharacter) -> Unit) : ListAdapter<
        FavoriteCharacter, CharactersListAdapter.CharacterHolder>(DiffCallback) {


    class CharacterHolder(
        private var binding: ItemFilterCharacterBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: FavoriteCharacter){
            binding.apply {
                itemFilterTitle.text = item.name
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterHolder {
        return CharacterHolder(ItemFilterCharacterBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: CharacterHolder, position: Int) {

        val currentItem = getItem(position)

        holder.itemView.setOnClickListener{ onItemClicked(currentItem)}

        holder.bind(currentItem)

    }

    companion object{
        private val DiffCallback = object : DiffUtil.ItemCallback<FavoriteCharacter>(){
            override fun areItemsTheSame(
                oldItem: FavoriteCharacter,
                newItem: FavoriteCharacter
            ): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(
                oldItem: FavoriteCharacter,
                newItem: FavoriteCharacter
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}