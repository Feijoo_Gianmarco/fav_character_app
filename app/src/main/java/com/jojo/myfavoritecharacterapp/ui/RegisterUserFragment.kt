package com.jojo.myfavoritecharacterapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.jojo.myfavoritecharacterapp.R
import com.jojo.myfavoritecharacterapp.databinding.FragmentRegisterUserBinding
import com.jojo.myfavoritecharacterapp.framework.FavCharacterApplication


class RegisterUserFragment : Fragment() {

    private var _binding: FragmentRegisterUserBinding? = null
    private val binding get() = _binding!!

    private val viewModel: FavoriteViewModel by activityViewModels {
        FavoriteViewModel.FavCharacterViewModelFactory(
            (activity?.application as FavCharacterApplication).database.repository()
        )

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterUserBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnRegisterUser.setOnClickListener {

            registerNewUser()

        }
    }

    private fun registerNewUser(){
        if(isEntryValid()){
            viewModel.addNewUser(
                binding.edtName.text.toString(),
                binding.edtUserName.text.toString(),
                binding.edtUserPassword.text.toString()
            )

            val action = RegisterUserFragmentDirections.actionRegisterUserFragmentToLoginFragment()
            findNavController().navigate(action)
        }
    }

    private fun isEntryValid():Boolean{
        return  viewModel.isEntryValid(
            binding.edtName.text.toString(),
            binding.edtUserName.text.toString(),
            binding.edtUserPassword.text.toString()
        )
    }


}