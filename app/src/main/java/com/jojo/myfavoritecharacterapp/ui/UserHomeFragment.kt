package com.jojo.myfavoritecharacterapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jojo.myfavoritecharacterapp.R
import com.jojo.myfavoritecharacterapp.databinding.FragmentUserHomeBinding
import com.jojo.myfavoritecharacterapp.framework.FavCharacterApplication
import com.jojo.myfavoritecharacterapp.ui.adapters.UserFavoritesAdapter

class UserHomeFragment : Fragment() {

    private var _binding: FragmentUserHomeBinding? = null
    private val binding get() = _binding!!

    private val viewModel: FavoriteViewModel by activityViewModels {
        FavoriteViewModel.FavCharacterViewModelFactory(
            (activity?.application as FavCharacterApplication).database.repository()
        )
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUserHomeBinding.inflate(inflater,container,false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = UserFavoritesAdapter{
            val actionToCharacterDetail = UserHomeFragmentDirections.actionUserHomeFragmentToCharacterDetailFragment(it.char_id)
            this.findNavController().navigate(actionToCharacterDetail)
        }

        binding.userListCharacters.layoutManager = LinearLayoutManager(this.context)

        val decorator = DividerItemDecoration(this.context, RecyclerView.VERTICAL)

        binding.userListCharacters.addItemDecoration(
            decorator
        )

        binding.userListCharacters.adapter = adapter


        viewModel.allUserFavCharacters().observe(this.viewLifecycleOwner) {
            items ->
            items.let {
                adapter.submitList(it)
            }
        }

        binding.btnAddNewCharacter.setOnClickListener {
            val actionAddNewCharacter = UserHomeFragmentDirections.actionUserHomeFragmentToAddCharacterFragment()
            findNavController().navigate(actionAddNewCharacter)
        }

    }




}