package com.jojo.myfavoritecharacterapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jojo.myfavoritecharacterapp.databinding.FragmentAddCharacterBinding
import com.jojo.myfavoritecharacterapp.framework.FavCharacterApplication
import com.jojo.myfavoritecharacterapp.ui.adapters.CharactersListAdapter

class AddCharacterFragment : Fragment() {

    private var _binding: FragmentAddCharacterBinding? = null
    private val binding get() = _binding!!

    private val viewModel: FavoriteViewModel by activityViewModels {
        FavoriteViewModel.FavCharacterViewModelFactory(
            (activity?.application as FavCharacterApplication).database.repository()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAddCharacterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = CharactersListAdapter {

            viewModel.addNewCharacter(it.name,it.id)

            val actionToUserHome =
                AddCharacterFragmentDirections.actionAddCharacterFragmentToUserHomeFragment()
            this.findNavController().navigate(actionToUserHome)
        }

        binding.listFilterCharacter.layoutManager = LinearLayoutManager(this.context)

        val decorator = DividerItemDecoration(this.context,RecyclerView.VERTICAL)

        binding.listFilterCharacter.addItemDecoration(
            decorator
        )

        binding.listFilterCharacter.adapter = adapter

        binding.btnSearch.setOnClickListener {

            searchByFilter(adapter)

        }


    }

    private fun searchByFilter(adapter: CharactersListAdapter){


        val inputText = binding.edtFilter.text.toString()

        viewModel.filterCharacterByName(inputText)

        viewModel.listFiltered.observe(this.viewLifecycleOwner){
            items ->
            items.let {
                adapter.submitList(it)
            }
        }

    }

}