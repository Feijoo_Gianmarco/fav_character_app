package com.jojo.myfavoritecharacterapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.jojo.myfavoritecharacterapp.databinding.FragmentLoginBinding
import com.jojo.myfavoritecharacterapp.framework.FavCharacterApplication

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val viewModelLogin: FavoriteViewModel by activityViewModels {
        FavoriteViewModel.FavCharacterViewModelFactory(
            (activity?.application as FavCharacterApplication).database.repository()
        )
    }

    //to capture the args
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container,false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.registerLink.setOnClickListener {

            val actionToRegister = LoginFragmentDirections.actionLoginFragmentToRegisterUserFragment()
            this.findNavController().navigate(actionToRegister)

        }

        binding.btnLogin.setOnClickListener {


            if(areFieldsBlank()){

                Toast.makeText(this.context,"Fields are blank!",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            viewModelLogin.logUser(binding.edtUserName.text.toString(),
                binding.edtUserPassword.text.toString())


        }


        val action = LoginFragmentDirections.actionLoginFragmentToUserHomeFragment()
        val nav = findNavController()

        viewModelLogin.user.observe(this.viewLifecycleOwner){
                user ->

            if(user != null) {
                clearFields()

                nav.navigate(action)
            }else{
                Toast.makeText(
                    this.context,
                    "Wrong credentials",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }


    }


    private fun areFieldsBlank(): Boolean{

        if(binding.edtUserName.text.toString().isBlank() ||
                binding.edtUserPassword.text.toString().isBlank()){
            return true
        }

        return false
    }

    private fun clearFields(){
        binding.edtUserName.text.clear()
        binding.edtUserPassword.text.clear()
    }

}