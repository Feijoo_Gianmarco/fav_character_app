package com.jojo.myfavoritecharacterapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jojo.myfavoritecharacterapp.databinding.ItemListFavCharacterBinding
import com.jojo.myfavoritecharacterapp.domain.UserFavCharacter

class UserFavoritesAdapter(private val onItemClicked: (UserFavCharacter) -> Unit) : ListAdapter<
        UserFavCharacter, UserFavoritesAdapter.FavCharacterViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavCharacterViewHolder {
        return FavCharacterViewHolder(ItemListFavCharacterBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: FavCharacterViewHolder, position: Int) {

        val currentItem = getItem(position)

        holder.itemView.setOnClickListener { onItemClicked(currentItem) }

        holder.bind(currentItem)

    }


    class FavCharacterViewHolder(private var binding: ItemListFavCharacterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: UserFavCharacter) {

            binding.apply {
                characterTitle.text = item.characterName

            }
        }

    }

    companion object{
        private val DiffCallback = object : DiffUtil.ItemCallback<UserFavCharacter>(){
            override fun areItemsTheSame(
                oldItem: UserFavCharacter,
                newItem: UserFavCharacter
            ): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(
                oldItem: UserFavCharacter,
                newItem: UserFavCharacter
            ): Boolean {
                return oldItem.id == newItem.id
            }

        }
    }


}