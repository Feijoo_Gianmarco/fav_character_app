package com.jojo.myfavoritecharacterapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import coil.load
import com.jojo.myfavoritecharacterapp.R
import com.jojo.myfavoritecharacterapp.databinding.FragmentCharacterDetailBinding
import com.jojo.myfavoritecharacterapp.domain.FavoriteCharacter
import com.jojo.myfavoritecharacterapp.framework.FavCharacterApplication


class CharacterDetailFragment : Fragment() {

    private var _binding: FragmentCharacterDetailBinding? = null
    private val binding get() = _binding!!

    lateinit var item: FavoriteCharacter

    private val viewModel: FavoriteViewModel by activityViewModels {
        FavoriteViewModel.FavCharacterViewModelFactory(
            (activity?.application as FavCharacterApplication).database.repository()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCharacterDetailBinding.inflate(inflater,container,false)
        return binding.root
    }

    private fun bindItem(item: FavoriteCharacter){
        val imgUri = item.img.toUri().buildUpon().scheme("https").build()
        binding.apply {
            title.text = item.name
            characterImage.load(imgUri)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = navArgs<CharacterDetailFragmentArgs>().value.itemId
        viewModel.retrieveCharacterDetail(id)


        viewModel.detailCharacter.observe(this.viewLifecycleOwner){
            selectedItem ->
            item = selectedItem
            bindItem(item)
        }


    }

}