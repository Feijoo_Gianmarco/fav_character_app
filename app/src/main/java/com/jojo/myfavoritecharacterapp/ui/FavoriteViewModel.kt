package com.jojo.myfavoritecharacterapp.ui

import androidx.lifecycle.*
import com.jojo.myfavoritecharacterapp.data.Repository
import com.jojo.myfavoritecharacterapp.data.UserDto
import com.jojo.myfavoritecharacterapp.data.UserFavCharacterDto
import com.jojo.myfavoritecharacterapp.domain.FavoriteCharacter
import com.jojo.myfavoritecharacterapp.domain.User
import com.jojo.myfavoritecharacterapp.domain.UserFavCharacter
import com.jojo.myfavoritecharacterapp.domain.usecase.UseCaseDetailCharacter
import com.jojo.myfavoritecharacterapp.domain.usecase.UseCaseFavoritesCharacters
import com.jojo.myfavoritecharacterapp.domain.usecase.UseCaseFilterCharacter
import com.jojo.myfavoritecharacterapp.domain.usecase.UseCaseUserLogin
import kotlinx.coroutines.*

class FavoriteViewModel(
    private val itemDao: Repository,
) : ViewModel() {

     val user = MutableLiveData<User?>()

    val detailCharacter = MutableLiveData<FavoriteCharacter>()
    val listFiltered = MutableLiveData<List<FavoriteCharacter>>()


    fun filterCharacterByName(characterName: String) {
        viewModelScope.launch {

            val result = withContext(Dispatchers.IO) {
                return@withContext UseCaseFilterCharacter(characterName).execute<List<FavoriteCharacter>>()
            }


            listFiltered.value = result

        }


    }

    fun retrieveCharacterDetail(id_item: Int) {

        viewModelScope.launch {

            val result = withContext(Dispatchers.IO) {
                return@withContext UseCaseDetailCharacter(id_item).execute<FavoriteCharacter>()
            }

            detailCharacter.value = result
        }


    }

    fun addNewCharacter(characterName: String, id_character: Int) {
        val newCharacter = newFavCharacter(characterName, id_character)
        registerCharacter(newCharacter)
    }

    private fun newFavCharacter(characterName: String, character_id: Int): UserFavCharacterDto {
        return UserFavCharacterDto(
            user_id = user.value!!.id,
            char_id = character_id,
            characterName = characterName
        )
    }

    private fun registerCharacter(character: UserFavCharacterDto) {
        viewModelScope.launch { itemDao.addNewCharacter(character) }
    }

    private fun newUser(name: String, userName: String, password: String): UserDto {
        return UserDto(name = name, userName = userName, userPassword = password)
    }

    private fun registerUser(user: UserDto) {
        viewModelScope.launch { itemDao.insertUser(user) }

    }

    fun addNewUser(name: String, userName: String, password: String) {
        val newUser = newUser(name, userName, password)
        registerUser(newUser)
    }

    fun isEntryValid(name: String, userName: String, password: String): Boolean {

        if (name.isBlank() || userName.isBlank() || password.isBlank()) {
            return false
        }


        return true
    }

    fun logOut(){
        user.value = null
    }


    fun logUser(username: String, password: String) {

        viewModelScope.launch {


            async {
                user.value = UseCaseUserLogin(itemDao,username,password).execute()
            }

        }



    }

    fun allUserFavCharacters(): LiveData<List<UserFavCharacter>>{

        val dataList = MutableLiveData<List<UserFavCharacter>>()
        viewModelScope.launch {

            val result = withContext(Dispatchers.IO) {
                return@withContext UseCaseFavoritesCharacters(itemDao,user.value!!.id).execute<List<UserFavCharacter>>()
            }

            dataList.postValue(result)
        }

        return dataList
    }

    class FavCharacterViewModelFactory(private val itemDao: Repository) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            if (modelClass.isAssignableFrom(FavoriteViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return FavoriteViewModel(itemDao) as T
            }
            throw IllegalArgumentException("UNKNOWN VIEWMODEL CLASS")
        }


    }

}